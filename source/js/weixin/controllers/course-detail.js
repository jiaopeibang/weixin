angular.module('jpb.weixin')

  .controller('courseDetailCtrl', ['$scope', '$rootScope', '$http', '$q', '$notification', '$stateParams',
    '$ionicScrollDelegate', '$timeout', '$wxAlert', '$pay', '$titleTool',
    function ($scope, $rootScope, $http, $q, $notification, $stateParams, $ionicScrollDelegate, $timeout, $wxAlert,
              $pay, $titleTool) {

      $titleTool.changeTitle('课程报名');
      var popup = null;
      var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
      $scope.user = $rootScope.user = $rootScope.user || {};

      if ($scope.user && $scope.user.profile) {
        delete $scope.user.verification;//防止页面之间共享
        $scope.user.profile.gender = $scope.user.profile.gender || 'male';
        $scope.oldUser = JSON.parse(JSON.stringify($scope.user));//copy
      } else if (!$stateParams.preview) {
        //非预览，提醒登陆，并退出
        $notification.show('登入已过期，页面将关闭', '2000');
        return $timeout(function () {
          WeixinJSBridge.call('closeWindow');
        }, 2000);
      }

      function init() {
        //获取课程
        var call = $http.get(server + '/api/course/' + $stateParams.id + '?userId=' + $scope.user._id);
        call.then(function (response) {
          var result = response.data;
          if (result.result !== 'SUCCESS') {
            return $notification.show('对不起，获取课程数据失败', '2000');
          }
          var course = result.data;
          //joined 为true，表单不可填
          course.joined = course.joined || $stateParams.preview;
          $scope.course = course;
          $scope.organization = $rootScope.organization || course.organization;
          $scope.limited = course.limit > 0 && course.members.length == course.limit;
        });
      }

      init();//购买后会再次执行，刷新状态

      //检查用户属性是不是被改变了
      function updateUserProfile(nPro, oPro) {
        if (oPro.mobile != nPro.mobile || oPro.realname != nPro.realname
          || oPro.parent.name != nPro.parent.name || oPro.gender != nPro.gender) {
          $http.post(server + '/api/wx/user/' + $scope.user._id, {user: $scope.user})
            .then(function (response) {
              var result = response.data;
              if (result.result !== 'SUCCESS') {
                $notification.show('对不起，更新身份信息失败', '2000');
              }
            }, function () {
              console.error('Request failed');
            });
        }
      }

      //检查验证码是否正确
      function verifyMobileCode() {
        return $http.get(server + '/api/verificationCodes/verify?mobile=' +
          $scope.user.profile.mobile + '&code=' + $scope.user.verification.code);
      }

      //微信统一下单
      function unifiedOrder(confirmed) {
        if (confirmed !== true) return;
        $pay.unifiedOrder($scope.course._id, $scope.user.profile.openid, $scope.organization._id)
          .then(function (response) {
            var result = response.data;
            if (result.result !== 'SUCCESS') {
              return $notification.show('对不起，生成订单失败', '2000');
            }
            callHtmlPay(result.data);
          }, function (e) {
            $notification.show('对不起，生成订单失败', '2000');
          });
      }

      //调用微信HTML5支付
      function callHtmlPay(sign) {
        var okFn = function () {
          var oPro = $scope.oldUser.profile, nPro = $scope.user.profile;
          updateUserProfile(nPro, oPro);//更新用户信息
          $scope.boughtFlag = true;//TODO 有没有必要
          $notification.show('报名成功！', '2000');
          init(true);//刷新数据
        };
        var failFn = function (prepayId) {
          $http.post(server + '/api/wx/orders/cancel', {prepayId: prepayId});
          $notification.show('对不起，支付失败，报名已取消', '2000');
        };
        var cancelFn = function (prepayId) {
          $http.post(server + '/api/wx/orders/cancel', {prepayId: prepayId});
          $notification.show('您已取消报名', '2000');
        };
        $pay.html5Pay(sign, okFn, failFn, cancelFn)
      }

      angular.extend($scope, {
        boughtFlag: false,//防止二次购买

        updateGender: function (value) {
          if (!$scope.course || $scope.course.joined)  return;
          angular.extend($scope.user.profile, {gender: value});//没报名，可变
        },

        signUp: function () {
          if (!$scope.course) {
            return $notification.show('页面数据尚未加载完毕，请稍后^_^！', '1000');
          }
          //加载完毕，且没有重复报名
          if ($scope.course.joined) {
            return $notification.show('您已经报名该课程啦，请不要重复操作^_^！', '1000');
          }
          if ($scope.limited) {
            return $notification.show('对不起，课程人数已满，请考虑其他课程', '1000');
          }
          verifyMobileCode().then(function (response) {
            var result = response.data;
            if (result.result !== 'SUCCESS') return $notification.show('验证码错误或已失效', '1000');
            $scope.backdrop = true;
            //开始支付流程
            var confirmWrap = $pay.confirm($scope, $scope.course.name, $scope.course.price.value);
            confirmWrap.promise.then(function (confirmed) {
              unifiedOrder(confirmed);
              $scope.backdrop = false;
            });
            popup = confirmWrap.popup;
          }, function () {
            $scope.backdrop = false;
            $notification.show('系统错误', '1000');
          });
        },

        //清除填写内容
        cleanContent: function (item) {
          if (item == 'realname') {
            $scope.user.profile.realname = '';
          } else if (item == 'parentName') {
            $scope.user.profile.parent.name = '';
          } else if (item == 'mobile') {
            $scope.user.profile.mobile = '';
          } else if (item == 'mobileCode') {
            $scope.user.verification.code = '';
          }
        },

        backToForm: function () {
          $ionicScrollDelegate.scrollBottom(false);

          $timeout(function () {
            if (!$scope.user.profile.realname) {
              document.getElementById('realname').focus();
            } else if (!$scope.user.profile.parent.name) {
              document.getElementById('parentName').focus();
            } else if (!$scope.user.profile.mobile) {
              document.getElementById('mobile').focus();
            }
          }, 500);
        }
      });

      $scope.$on('$destroy', function () {
        if (popup) {
          popup.close();
        }
      })
    }]);
