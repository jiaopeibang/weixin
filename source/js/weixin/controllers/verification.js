'use strict';

angular.module('jpb.weixin')

  // 验证码
  .controller('VerificationCodeController', ['$scope', '$http',
    function ($scope, $http) {
      var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;

      $scope.sendValue = '发送验证码';
      function startTimer(sectionId) {
        $('.verification-timer timer')[0].start();
      }

      $scope.$on('$destroy', function () {
        $scope.data.$verifying = false;
      });

      _.extend($scope, {
        data: {
          $verificationType: '', //0: phone, 1: audio
          $verifying: false,
          verifyCodeError: false
        },
        //获取验证码
        generateCode: function (_user, type) {
          $scope.hadGenerate = true;
          $scope.data.$verifying = true;
          $scope.data.$verifyType = type;
          var mobile = _user.profile.mobile;
          if (mobile) {
            var url = server + '/api/verificationCodes/generate?type=' + type + '&mobile=' + mobile;
            $http.get(url).then(function (result) {
              startTimer('verification-timer');
            }, function (error) {
              console.log(error);
            });
          }
        },
        //语音验证码
        genVerificationCode: function (_user, type) {
          $scope.data.$verifying = true;
          $scope.data.$verifyType = type;
          var mobile = _user.profile.mobile;
          if (mobile) {
            var url = server + '/api/verificationCodes/generate?type=' + type + '&mobile=' + mobile;
            $http.get(url).then(function (result) {
              startTimer('verification-timer');
            }, function (error) {
              console.log(error);
            });
          }
        },
        timerFinished: function () {
          $scope.$apply(function () {
            $scope.sendValue = '重新发送';
            $scope.data.$verifying = false;
          });
        },

        cleanVerificationCode: function () {
          $scope.user.verificationCode = '';
          setTimeout("document.getElementById('mobileCode').focus()", 10);
        }
      });
    }
  ]);
