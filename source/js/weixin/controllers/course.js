angular.module('jpb.weixin')
  .controller('courseCtrl', ['$scope', '$rootScope', '$state', '$stateParams', '$notification',
    '$http', 'user', '$titleTool',

    function ($scope, $rootScope, $state, $stateParams, $notification, $http, user, $titleTool) {

      $titleTool.changeTitle('课程一览');
      if (!user) return $notification.show('服务器数据加载异常，请稍后再试');

      var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
      $rootScope.user = user;

      var coursePromise = $http.get(server + '/api/courses?appid=' + $stateParams.appid);
      coursePromise.then(function (response) {
        var result = response.data;
        if (result.result !== 'SUCCESS') {
          alert('加载数据出现错误，请联系客服');
        }
        $scope.courses = result.data;
      }, function () {
        alert('加载数据出现错误，请联系客服');
      });
      $scope.goDetail = function (id) {
        $state.go('wx.courseDetail', {id: id, preview: $stateParams.preview});
      };

      $http.get(server + '/api/wx/organization/' + $stateParams.appid)
        .then(function (response) {
          var result = response.data;
          $rootScope.organization = result && result.data;
        });
    }
  ]);
