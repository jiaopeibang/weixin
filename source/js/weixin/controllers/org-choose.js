angular.module('jpb.weixin')

  .controller('orgChooseCtrl', ['$notification', '$http', '$timeout', '$rootScope', '$scope', '$stateParams',
    'user', '$titleTool',
    function ($notification, $http, $timeout, $rootScope, $scope, $stateParams, user, $titleTool) {

      $titleTool.changeTitle('绑定机构');
      var serverBaseUrl = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
      var appid = $stateParams.appid, loading = true;
      $scope.user = $rootScope.user = user;
      $scope.container = {};

      if (!$scope.user) return printError('获取用户数据出错，请尝试重新关注哦！');

      $http.get(serverBaseUrl + '/api/wx/organization/sub/' + appid)
        .then(function (res) {
          loading = false;
          var result = res.data;
          if (result.result !== 'SUCCESS') return printError('数据加载错误');

          $scope.subOrgs = result.data;
          $scope.container.selectedOrg = $scope.subOrgs[0];
        }, printError);

      angular.extend($scope, {
        submit: function () {
          if (loading) return printError('数据正在加载中，请稍后^_^');
          if (user.profile.orgId && user.profile.parentOrgId)
            return printError('请不要重复绑定!');
          if (!$scope.container.selectedOrg || !$scope.container.selectedOrg._id)
            return printError('请选择您要绑定的机构');

          $http.post(serverBaseUrl + '/api/wx/user/modify/org',
            {userId: user._id, orgId: $scope.container.selectedOrg._id}
          ).then(function () {
            $notification.show('绑定成功！', '2000');
            return $timeout(function () {
              WeixinJSBridge.call('closeWindow');
            }, 2000);
          }, printError);
        }
      });

      function printError(content) {
        $notification.show(content || '对不起，程序发生错误', 2000);
      }
    }]);
