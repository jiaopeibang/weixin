angular.module('jpb.weixin')

  .config(['$urlRouterProvider', '$stateProvider',
    function ($urlRouterProvider, $stateProvider) {

      $stateProvider
        .state('wx', {
          url: '/wx',
          templateUrl: 'templates/common/navigator.html'
        })
        .state('wx.course', {
          url: '/course/list?code&appid&preview',
          templateUrl: 'templates/weixin/course.html',
          controller: 'courseCtrl',
          resolve: {
            user: ['$rootScope', '$stateParams', '$http', function ($rootScope, $stateParams, $http) {
              if ($stateParams.preview) return {};//client preview
              if ($rootScope.user) return $rootScope.user;
              var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
              return $http.get(server + '/api/wx/user?appid=' + $stateParams.appid + '&code=' + $stateParams.code)
                .then(function (response) {
                  return response.data && response.data.data;
                });
            }]
          }
        })
        .state('wx.courseDetail', {
          url: '/course/detail?id&preview',
          templateUrl: 'templates/weixin/detail.html',
          controller: 'courseDetailCtrl'
        })
        .state('wx.chooseOrg', {
          url: '/chooseOrg?appid&code',
          templateUrl: 'templates/weixin/choose-org.html',
          controller: 'orgChooseCtrl',
          resolve: {
            user: ['$rootScope', '$stateParams', '$http', function ($rootScope, $stateParams, $http) {
              if ($rootScope.user) return $rootScope.user;
              var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
              return $http.get(server + '/api/wx/user?appid=' + $stateParams.appid + '&code=' + $stateParams.code)
                .then(function (response) {
                  return response.data && response.data.data;
                });
            }]
          }
        });
    }]);
