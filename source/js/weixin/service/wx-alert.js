angular.module('jpb.weixin')
  /**
   * simple diy alert for wx
   */
  .factory('$wxAlert', ['$ionicPopup', function ($ionicPopup) {
    return function (options) {
      var title, tapFn, thenFn;
      var emptyFn = function () {
      };

      if (typeof options === 'string') {
        title = options;
      } else {
        title = options.title;
        tapFn = options.tapFn;
        thenFn = options.thenFn;
      }
      $ionicPopup.alert({
        title: title,
        cssClass: 'popup-alert',
        buttons: [{
          text: '确定',
          type: 'button-default button-clear button-positive',
          onTap: tapFn || emptyFn
        }]
      }).then(thenFn || emptyFn);
    }
  }]);
