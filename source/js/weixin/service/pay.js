angular.module('jpb.weixin')
  .factory('$pay', ['$http', '$q', '$ionicPopup', function ($http, $q, $ionicPopup) {

    var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
    return {
      /**
       * promise style, return ['OK'|'CANCEL']
       * @param scope
       * @param orderName
       * @param cost
       * @returns {*}
       */
      confirm: function (scope, orderName, cost) {
        var defer = $q.defer();
        var template = "<div class='row'>" +
          "<div class='left cell'>课程名称：</div>" +
          "<div class='right cell'>" + orderName + "</div>" +
          "</div>" +
          "<div class='row'>" +
          "<div class='left cell'>金额总计：</div>" +
          "<div class='right cell'>&yen;&nbsp;" + cost + "</div>" +
          "</div>";
        var popup = $ionicPopup.confirm({
          title: '<div class="order-head">订单详情</div>',
          scope: scope,
          template: template,
          cssClass: 'confirm-order',
          buttons: [{
            text: '取消',
            onTap: function () {
              defer.resolve(false);
            }
          }, {
            text: '支付',
            type: 'button-positive',
            onTap: function (event) {
              defer.resolve(true);
            }
          }]
        });
        return {promise: defer.promise, popup: popup};
      },

      unifiedOrder: function (courseId, openid, orgId) {
        return $http.post(server + '/api/wx/orders/generate', {
          courseId: courseId,
          openid: openid,
          orgId: orgId
        });
      },

      html5Pay: function (sign, successFn, failFn, cancelFn) {
        WeixinJSBridge.invoke('getBrandWCPayRequest', sign,
          function (res) {
            var prepayId = sign.package.substring(10);
            if (res.err_msg == "get_brand_wcpay_request:ok") {
              successFn();
            } else if (res.err_msg == 'get_brand_wcpay_request：cancel') {
              cancelFn(prepayId);
            } else {
              failFn(prepayId);
            }
          }
        );
      }
    };
  }]);
