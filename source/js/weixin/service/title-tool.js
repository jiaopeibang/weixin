angular.module('jpb.weixin')
  .factory('$titleTool', [function () {

    return {
      changeTitle: function (title) {
        if (navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
          var body = document.getElementsByTagName('body')[0];
          document.title = title;
          var iframe = document.createElement("iframe");
          iframe.setAttribute("src", "/favicon.ico");
          iframe.addEventListener('load', function () {
            setTimeout(function () {
              iframe.removeEventListener('load');
              document.body.removeChild(iframe);
            }, 0);
          });
          document.body.appendChild(iframe);
        }
      }
    }

  }]);
