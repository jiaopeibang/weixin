(function(angular) {
	"use strict";

	var Jpb = {
		util: {}
	};
	angular.extend(Jpb.util, {
		/**
		 * Copy stuff selectively from `src` according to `fields`.
		 * @param {Array} fields An array of strings.
		 * @param {Object} src
		 */
		selectiveCopy: function(fields, src) {
			var o = {};

			angular.forEach(fields, function(v) {
				var cp = angular.copy(src[v]);

				if (cp !== undefined) {
					o[v] = cp;
				}
			});

			return o;
		},
		/**
		 * Merge any difference into dst object.
		 * @param {Object} dst
		 * @returns {Object} The dst object itself.
		 */
		merge: function(dst) {
			angular.forEach(arguments, function(v) {
				if (v !== dst) {
					angular.forEach(v, function(vv, kk) {
						if (kk.substr(0, 2) === "$$") {// why
							angular.forEach(vv, function(vvv, kkk) {
								if (dst.hasOwnProperty(kkk) && angular.isArray(vvv)) {
									dst[kkk] = dst[kkk].concat(vvv);
								} else {
									dst[kkk] = vvv;
								}
							});
						} else {
							if (vv === undefined) {
								delete dst[kk];
							} else {
								dst[kk] = vv;
							}
						}
					});
				}
			});
			return dst;
		},
		/**
		 * Get an object with changed stuff with depth of 1. NOTE that any key named in the "$$xxx" pattern will be ignored.
		 * @param {Object} src
		 * @param {Object} objWithChanges
		 */
		diff: function(src, objWithChanges) {
			var o = {};

			angular.forEach(objWithChanges, function(v, k) {
				if (!/^\$\$/.test(k) && k !== "lastmodified" && !angular.equals(v, src[k])) {
					o[k] = v;
				}
			});

			return angular.isEmpty(o) ? null : o;
		},
		/**
		 * Get data from context object down the path.
		 * @example
		 * <code>
		 * var o = {
	 *   p1: {
	 *     p1_1: false,
	 *     p1_2: [0, 1, 2, 3, {
	 *       p1_2_4_1: "you got me in array"
	 *     }]
	 *   }
	 * };
		 * TierOnes.util.byPath("p1", o);// object
		 * TierOnes.util.byPath("p1.p1_1", o);// false
		 * TierOnes.util.byPath("p1.p1_1.p1_1_3", o);// undefined
		 * TierOnes.util.byPath("p1.p1_2", o);// array
		 * TierOnes.util.byPath("p1.p1_2.1", o);// 1
		 * TierOnes.util.byPath("p1.p1_2.4", o);// object
		 * TierOnes.util.byPath("p1.p1_2.4.p1_2_4_1", o);// "you got me in array"
		 * </code>
		 * @param {String} path
		 * @param {Object} [context=window]
		 */
		byPath: function(path, context) {
			var o = context || window,
				paths = path.split("."),
				p = paths.shift();

			while (o && p) {
				o = o[p];
				p = paths.shift();
			}

			return p ? undefined : o;
		},
		/**
		 * Set data to context object according to path, anything along the path will be created as an object if not exist.
		 * @example
		 * <code>
		 * var o = {
	 *   p1: {
	 *     p1_1: {}
	 *   }
	 * };
		 * TierOnes.util.byPathSet("p1.p1_1.p1_1_1", o, "foo");
		 * TierOnes.util.byPathSet("p1.p1_2.p1_2_1", o, [1,2,3]);
		 * </code>
		 * @param {String} path
		 * @param {Object} context `window` if falsy null/undefined/false/0/""/NaN
		 * @param {*} data
		 */
		byPathSet: function(path, context, data) {
			if (arguments.length < 3) {
				return;
			}

			var o = context || window,
				paths = path.split("."),
				lastP = paths.pop(),
				p = paths.shift();

			while (p) {
				if (!o[p]) {// create if not exist
					o[p] = {};
				}

				o = o[p];
				p = paths.shift();
			}

			o[lastP] = data;
		},
		/**
		 * Add data to a set in context object according to path.
		 * @example
		 * <code>
		 * var o = {
	 *   p1: {
	 *     p1_1: [1, 2]
	 *   }
	 * };
		 * TierOnes.util.byPathAddToSet("p1.p1_1", o, 2);// will not be added
		 * TierOnes.util.byPathAddToSet("p1.p1_1", o, 3);
		 * TierOnes.util.byPathAddToSet("p2", o, "p2");
		 *
		 * `o` becomes {
	 *   p1: {
	 *     p1_1: [1, 2, 3]
	 *   },
	 *   p2: ["p2"]
	 * }
		 * </code>
		 * @param {String} path
		 * @param {Object} context
		 * @param {*} data
		 */
		byPathAddToSet: function(path, context, data) {
			var o = context || window,
				paths = path.split("."),
				lastP = paths.pop(),
				p = paths.shift();

			while (p) {
				if (o[p] === undefined) {
					o[p] = {};
				}

				if (!angular.isObject(o[p])) {// mongo will report error when doing on already exist stuff
					return;
				}

				o = o[p];
				p = paths.shift();
			}

			var arr = o[lastP];
			if (arr === undefined) {
				arr = [];
				o[lastP] = arr;
			}

			if (!angular.isArray(arr)) {// mongo will report error when you do $addToSet on something exist but not an array
				return;
			}

			if (arr.some(function(v) {// angular will not add the data if there already exists one that is totally the same
					return angular.equals(v, data);
				})) {
				return;
			}

			arr.push(data);
		},
		/**
		 * Pull data from context object according to path and data.
		 * @example
		 * <code>
		 * var o = {
	 *   p1: {
	 *     p1_1: [1, 2, 3],
	 *     p1_2: [{
	 *       a: 1, b: 2
	 *     }, {
	 *       a: 1, b: 3
	 *     }, {
	 *       a: 2, b: 4
	 *     }]
	 *   }
	 * };
		 * TierOnes.util.byPathPull("p1.p1_1", o, 2);
		 * TierOnes.util.byPathUnset("p1.p1_2", o, {a: 1});
		 * TierOnes.util.byPathPull("p1.p1_3", o, 3);// does nothing
		 *
		 * `o` becomes {
	 *   p1: {
	 *     p1_1: [1, 3],
	 *     p1_2: [{
	 *       a: 2, b: 4
	 *     }]
	 *   }
	 * }
		 * </code>
		 * @param {String} path
		 * @param {Object} context
		 * @param {*} data
		 */
		byPathPull: function(path, context, data) {
			var arr = TierOnes.util.byPath(path, context);

			if (!angular.isArray(arr)) {// angular will report error when performing $pull on something that is not an array
				return;
			}

			var i = 0,
				dataIsSimple = !angular.isObject(data) && !angular.isArray(data);
			while (i < arr.length) {// do not cache arr.length cause arr.length will change during the operation
				var item = arr[i],
					needPull = true;
				if (dataIsSimple) {
					needPull = item === data;
				} else {// TODO mongo pull is actually a query, BUT here we only support simple data or subset, all $xx is NOT yet supported
					for (var k in data) {
						if (data.hasOwnProperty(k)) {
							if (!angular.equals(data[k], item[k])) {
								needPull = false;
								break;
							}
						}
					}
				}

				if (needPull) {
					arr.splice(i, 1);
				} else {
					i += 1;
				}
			}
		},
		/**
		 * Unset data to context object according to path.
		 * @example
		 * <code>
		 * var o = {
	 *   p1: {
	 *     p1_1: {
	 *       p1_1_1: "foo"
	 *     }
	 *   }
	 * };
		 * TierOnes.util.byPathUnset("p1.p1_1.p1_1_1", o);
		 * TierOnes.util.byPathUnset("p1.p1_2.p1_2_1", o);// does nothing
		 * </code>
		 * @param {String} path
		 * @param {Object} context
		 */
		byPathUnset: function(path, context) {
			var o = context || window,
				paths = path.split("."),
				lastP = paths.pop(),
				p = paths.shift();

			while (o && p) {
				o = o[p];
				p = paths.shift();
			}

			try {// IE will throw exception even if you're deleting your own stuff
				if (o && lastP in o) {
					o[lastP] = null;
					delete o[lastP];
				}
			} catch (ex) {}
		},
		/**
		 * un-flatten an object that can be recognized by mongodb into normal JS objects.
		 * @example
		 * <code>
		 * TierOnes.util.unflatten({
	 *   a: "a",
	 *   "b.c": "b.c",
	 *   "b.d": {
	 *     bd: "bd"
	 *   }
	 * });
		 * // will result in an object {
	 *   a: "a",
	 *   b: {
	 *     c: "b.c",
	 *     d: {
	 *       bd: "bd"
	 *     }
	 *   }
	 * }
		 * </code>
		 * @param src
		 * @returns {{}}
		 */
		unflatten: function(src) {
			var o = {};

			angular.forEach(src, function(v, k) {
				var lastO = o,
					paths = k.split("."),
					lastP = paths.pop(),
					p = paths.shift();

				while (p) {
					if (!lastO[p]) {
						lastO[p] = {};
					}

					lastO = lastO[p];
					p = paths.shift();
				}

				lastO[lastP] = v;
			});

			return o;
		},
		/**
		 * Perform mongo update to local object.
		 *
		 * @param o
		 * @param update
		 */
		mongoUpdate: function(o, update) {
			angular.forEach(update, function(v, k) {
				switch (k) {
					case "$set":
						TierOnes.util.mongoSet(o, v);
						break;
					case "$unset":
						TierOnes.util.mongoUnset(o, v);
						break;
					case "$addToSet":
						TierOnes.util.mongoAddToSet(o, v);
						break;
					case "$pull":
						TierOnes.util.mongoPull(o, v);
						break;
					default:
						break;
				}
			});
		},
		/**
		 * Perform mongo's $set locally.
		 * @example
		 * <code>
		 * var o = {
	 *   a: [1, 2],
	 *   b: {
	 *     b1: "b.b1"
	 *   }
	 * };
		 * TierOnes.util.mongoSet(o, {
	 *   a: ["a", "b", "c"],// replace `o.a`
	 *   "b.b2": "b.b2",// add "b2" to `o.b`
	 *   "c.c1": "c.c1"// add `o.c`, `o.c.c1`
	 * });
		 * // o becomes
		 * {
	 *   a: ["a", "b", "c"],// replaced
	 *   b: {
	 *     b1: "b.b1",
	 *     b2: "b.b2"// added
	 *   },
	 *   c: {// added
	 *     c1: "c.c1"
	 *   }
	 * }
		 * </code>
		 * @param {Object} o
		 * @param {Object} oSet
		 */
		mongoSet: function(o, oSet) {
			angular.forEach(oSet, function(v, k) {
				TierOnes.util.byPathSet(k, o, v);
			});
		},
		/**
		 * Perform mongo's $unset locally.
		 * @example
		 * <code>
		 * var o = {
	 *   a: [1, 2],
	 *   b: {
	 *     b1: "b.b1",
	 *     b2: "b.b2"
	 *   }
	 * };
		 * TierOnes.util.mongoUnset(o, {
	 *   a: 1,// remove `a`
	 *   "b.b2": 1,// remove `o.b.b2`
	 *   "c.c1": 1// does nothing
	 * });
		 * // o becomes
		 * {
	 *   b: {
	 *     b1: "b.b1"
	 *   }
	 * }
		 * </code>
		 * @param {Object} o
		 * @param {Object} oUnset
		 */
		mongoUnset: function(o, oUnset) {
			angular.forEach(oUnset, function(v, k) {
				TierOnes.util.byPathUnset(k, o);
			});
		},
		/**
		 * Perform mongo's $addToSet locally.
		 * @example
		 * <code>
		 * var o = {
	 *   a: [1, 2],
	 *   b: {
	 *     b1: "b1"
	 *   }
	 * };
		 * TierOnes.util.mongoAddToSet(o, {
	 *   a: 3,
	 *   "b.b2": "b2"
	 * });
		 * // o becomes
		 * {
	 *   a: [1, 2, 3],
	 *   b: {
	 *     b1: "b1",
	 *     b2: ["b2"]
	 *   }
	 * }
		 * </code>
		 * @param {Object} o
		 * @param {Object} addToSet
		 */
		mongoAddToSet: function(o, addToSet) {
			angular.forEach(addToSet, function(v, k) {
				TierOnes.util.byPathAddToSet(k, o, v);
			});
		},
		/**
		 * Perform mongo's $pull locally. TODO NOT fully implemented!
		 * @example
		 * <code>
		 * var o = {
	 *   p1: {
	 *     p1_1: [1, 2, 3],
	 *     p1_2: [{
	 *       a: 1, b: 2
	 *     }, {
	 *       a: 1, b: 3
	 *     }, {
	 *       a: 2, b: 4
	 *     }]
	 *   }
	 * };
		 * TierOnes.util.mongoPull(o, {
	 *   "p1.p1_1": 2,
	 *   "p1.p1_2": {
	 *     a: 1
	 *   }
	 * });
		 *
		 * `o` becomes {
	 *   p1: {
	 *     p1_1: [1, 3],
	 *     p1_2: [{
	 *       a: 2, b: 4
	 *     }]
	 *   }
	 * }
		 * </code>
		 *
		 * @param o
		 * @param pull
		 */
		mongoPull: function(o, pull) {
			angular.forEach(pull, function(v, k) {
				TierOnes.util.byPathPull(k, o, v);
			});
		},

		setValueByPath: function(path, val, obj) {
			var fields = path.split("."),
				result = obj;

			for (var i = 0, n = fields.length; i < n && result !== undefined; i++) {
				var field = fields[i];
				if (i === n - 1) {
					result[field] = val;
				} else {
					if (typeof result[field] === "undefined" || !_.isObject(result[field])) {
						result[field] = {};
					}
					result = result[field];
				}
			}
		},
		optObject: function(obj) {
			angular.forEach(obj, function(_v, _k) {
				if (_k.indexOf(".") > 0) {
					TierOnes.util.setValueByPath(_k, _v, obj);
				}
			});
		},
		retrieveSiteName: function(url) {
			if(angular.isEmpty(url)){
				return url;
			}
			var parts = url.split("."), plen = parts.length, siteName, ind = 0;
			if(plen>3){
				ind = plen-3;
			}else if(plen>2){
				ind = plen-2;
			}else{
				ind = plen-1;
			}
			siteName = parts[ind];
			while(ind>0 && ["com","cn","net"].indexOf(siteName) > -1) {
				ind--;
				siteName = parts[ind];
			}
			if(!angular.isEmpty(siteName)){
				var nameparts = siteName.split("://");
				if(nameparts && nameparts.length>0){
					siteName = nameparts[nameparts.length-1];
				}
			}
			return siteName;
		},
		/**
		 * Parse arguments object into an array.
		 * @param {Arguments} args
		 * @param {Number} [begin]
		 * @param {Number} [end]
		 * @returns {Array}
		 */
		argsToArr: function(args, begin, end) {
			return Array.prototype.slice.call(args, begin, end);
		},

		base64: (function() {
			/**
			 * The base64 en/decoder that supports UTF8 characters like Chinese.
			 * @namespace
			 * @name TierOnes.util.base64
			 */
			var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

			function _encode(input) {
				input = input.replace(/\r\n/g, "\n");

				var output = [],
					i, c, l = input.length;

				for (i = 0; i < l; i++) {
					c = input.charCodeAt(i);

					if (c < 128) {
						output.push(String.fromCharCode(c));
					} else if (c > 127 && c < 2048) {
						output.push(String.fromCharCode((c >> 6) | 192), String.fromCharCode((c & 63) | 128));
					} else {
						output.push(String.fromCharCode((c >> 12) | 224), String.fromCharCode(((c >> 6) & 63) | 128), String.fromCharCode((c & 63) | 128));
					}
				}

				return output.join("");
			}

			function _decode(input) {
				var output = [],
					i = 0, l = input.length,
					c1 = 0, c2 = 0, c3 = 0;

				while (i < l) {
					c1 = input.charCodeAt(i);

					if (c1 < 128) {
						output.push(String.fromCharCode(c1));
						i++;
					} else if (c1 > 191 && c1 < 224) {
						c2 = input.charCodeAt(i + 1);
						output.push(String.fromCharCode(((c1 & 31) << 6) | (c2 & 63)));
						i += 2;
					} else {
						c2 = input.charCodeAt(i + 1);
						c3 = input.charCodeAt(i + 2);
						output.push(String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)));
						i += 3;
					}
				}

				return output.join("");
			}

			return {
				/**
				 * Encode a string into UTF8 base64 string.
				 * @param {String} input
				 * @returns {String}
				 */
				encode: function(input) {
					input = _encode(input);

					var output = [],
						i = 0, l = input.length,
						c1, c2, c3,
						n1, n2, n3, n4;

					while (i < l) {
						c1 = input.charCodeAt(i++);
						c2 = input.charCodeAt(i++);
						c3 = input.charCodeAt(i++);

						n1 = c1 >> 2;
						n2 = ((c1 & 3) << 4) | (c2 >> 4);
						n3 = ((c2 & 15) << 2) | (c3 >> 6);
						n4 = c3 & 63;

						if (isNaN(c2)) {
							n3 = 64;
							n4 = 64;
						} else if (isNaN(c3)) {
							n4 = 64;
						}

						output.push(CHARS.charAt(n1), CHARS.charAt(n2), CHARS.charAt(n3), CHARS.charAt(n4));
					}

					return output.join("");
				},
				/**
				 * Decode the string into normal state.
				 * @param {String} input
				 * @returns {String}
				 */
				decode: function(input) {
					input = input.replace(/[^A-Za-z0-9=\+\/]/g, "");

					var output = [],
						i = 0, l = input.length,
						c1, c2, c3,
						n1, n2, n3, n4;

					while (i < l) {
						n1 = CHARS.indexOf(input.charAt(i++));
						n2 = CHARS.indexOf(input.charAt(i++));
						n3 = CHARS.indexOf(input.charAt(i++));
						n4 = CHARS.indexOf(input.charAt(i++));

						c1 = (n1 << 2) | (n2 >> 4);
						c2 = ((n2 & 15) << 4) | (n3 >> 2);
						c3 = ((n3 & 3) << 6) | n4;

						output.push(String.fromCharCode(c1));

						if (n3 != 64) {
							output.push(String.fromCharCode(c2));
						}
						if (n4 != 64) {
							output.push(String.fromCharCode(c3));
						}
					}

					return _decode(output.join(""));
				}
			};
		}())
	});

// export
	window.Jpb = Jpb;
}(angular));
