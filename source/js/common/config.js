'use strict';

angular.module('jpb.common')

  .config(['$stateProvider', '$urlRouterProvider', '$ionicConfigProvider',
    function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
      // let's turn off swipe to go back
      $ionicConfigProvider.views.swipeBackEnabled(false);

      // 最多缓存5个视图
      $ionicConfigProvider.views.maxCache(5);

      // 配置导航返回按钮默认图标样式
      $ionicConfigProvider.backButton.text('').icon('ion-chevron-left');
      $ionicConfigProvider.backButton.previousTitleText(false);
      $ionicConfigProvider.views.transition('none');

      $ionicConfigProvider.templates.maxPrefetch(1);

      $stateProvider
        .state('init', {
          url: '/init',
          controller: 'InitController',
          data: {acl: {signIn: false}}
        })
        .state('navigator', {
          abstract: true,
          templateUrl: 'templates/common/navigator.html'
        });

      // if none of the above states are matched, use this as the fallback
      //$urlRouterProvider.otherwise('/init');
    }
  ]);
