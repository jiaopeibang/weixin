'use strict';

angular.module('jpb.common')

  .factory('$audio', ['$cordovaMedia', '$q', '$audioHtml5',
    function ($cordovaMedia, $q, $audioHtml5) {
      return {
        newMedia: function (filePath, afterStopRecord) {
          if (!window.Media) return $audioHtml5.newMedia(filePath);

          var newFlag = !filePath,
            media,
            startTimestamp,
            duration = 0,
            result;
          if (newFlag) {
            var baseFileName = "jpb" + Date.now(), fileName, mimeType;
            if (ionic.Platform.isIOS()) {
              fileName = baseFileName + ".m4a";
              mimeType = "audio/m4a";
              filePath = cordova.file.tempDirectory + fileName;
              if (filePath.indexOf('file://') > -1) filePath = filePath.substring(7);
            } else if (ionic.Platform.isAndroid()) {
              fileName = baseFileName + ".amr";
              mimeType = "audio/amr";
              filePath = "/sdcard/" + fileName;
            }
          }

          media = $cordovaMedia.newMedia(newFlag ? fileName : filePath);
          media.then(function () {
            console.log("Media Success");
            afterStopRecord && afterStopRecord();
          }, function (error) {
            console.log("Media error: " + angular.toJson(error));
            console.log("Media error code: " + error.code);
          }, function (result) {
            console.log("Media status: " + angular.toJson(result));
          });

          result = {
            getFileName: function () {
              return filePath;
            },
            getMimeType: function () {
              return mimeType;
            },
            getCurrentPosition: function (cb) {
              media.media.getCurrentPosition(cb);
            },
            getDuration: function (force) {
              if (force) {
                var defer = $q.defer(),
                  counter = 0,
                  stopFlag = false,
                  timerDur = setInterval(function () {
                    counter = counter + 100;
                    if (counter > 2000) {
                      clearInterval(timerDur);
                      stopFlag = true;
                    }
                    var dur = media.getDuration() || 0;
                    if (dur > 0) {
                      clearInterval(timerDur);
                      stopFlag = true;
                    }
                    if (stopFlag) {
                      defer.resolve(dur);
                    }
                  }, 100);
                return defer.promise;
              }
              return media.getDuration();
            },
            getDuration2: function (force) {
              console.log("getDuration begin");
              // Get duration
              var defer = $q.defer();
              if (force) {
                var counter = 0,
                  stopFlag = false,
                  timerDur;
                timerDur = setInterval(function () {
                  console.log("timerDur:" + counter);
                  counter = counter + 100;
                  if (counter > 2000) {
                    clearInterval(timerDur);
                    stopFlag = true;
                  }
                  var dur = media.getDuration();
                  if (dur > 0) {
                    clearInterval(timerDur);
                    stopFlag = true;
                  }
                  if (stopFlag) {
                    console.log("duration:" + dur);
                    defer.resolve(dur);
                  }
                }, 100);
              } else {
                return $q.when(duration);
              }
              console.log("getDuration");
              console.log(defer.promise);
              return defer.promise;
            },
            play: function () {
              media.play();
            },
            pause: function () {
              media.pause();
            },
            resume: function () {
              media.play();
            },
            stop: function () {
              media.stop();
            },
            seekTo: function (timing) {
              media.seekTo(timing);
            },
            setVolume: function (volume) {
              media.setVolume(volume);
            },
            startRecord: function (callback) {
              callback = callback || function () {
                };
              console.log("start record");
              media.startRecord();
              startTimestamp = Date.now();
              return callback();
            },
            cancelRecord: function () {
              media.stopRecord();
              media.release();
            },
            stopRecord: function (callback) {
              callback = callback || function () {
                };
              media.stopRecord();
              duration = Date.now() - startTimestamp;
              return callback({fullPath: filePath, duration: duration, mimeType: mimeType});
            },
            pauseRecord: function () {
              media.pauseRecord();
            },
            resumeRecord: function () {
              media.resumeRecord();
            },
            isPaused: function () {
              return media.getStatus() == 3;
            },
            isStopped: function () {
              return media.getStatus() == 4;
            },
            release: function () {
              media.release();
            },
            setStatusChangeHandler: function (handler) {
              media.onStatusChange = function (status) {
                handler(status);
              };
            }
          };

          return result;
        }
      };
    }
  ])

  .factory('$audioHtml5', ['$q', function ($q) {
    return {
      newMedia: function (filePath) {
        var duration = 0;
        var media;

        var resultObj = {
          getCurrentPosition: function (cb) {
            cb(media.currentTime);
          },
          getDuration: function (force) {
            if (force) {
              var defer = $q.defer();
              var counter = 0, stopflag = false;
              var timerDur = setInterval(function () {
                counter = counter + 100;
                if (counter > 2000) {
                  clearInterval(timerDur);
                  stopflag = true;
                }
                var dur = media.duration || 0;
                if (dur > 0) {
                  clearInterval(timerDur);
                  stopflag = true;
                }
                if (stopflag) {
                  defer.resolve(dur);
                }
              }, 100);
              return defer.promise;
            }
            return media.duration;
          },
          play: function () {
            media.play();
          },
          pause: function () {
            media.pause();
          },
          resume: function () {
            media.play();
          },
          stop: function () {
            media.pause();
            handler({type: 'pause', jsInvoke: true});
          },
          seekTo: function (timing) {
            media.currentTime = timing / 1000;
          },
          setVolume: function (volume) {
            media.volume = volume;
          },
          isPaused: function () {
            return media.paused;
          },
          isStopped: function () {
            return media.ended;
          },
          release: function () {
            media.src = '';
            media = null;
          },
          setStatusChangeHandler: function (handler) {
            media.onStatusChange = function (status, isTrusted) {
              handler(status, isTrusted);
            };
          }
        };

        media = new Audio(filePath);
        var handler = function (event) {
          var status = 0;
          switch (event.type) {
            case 'play':
              status = 2;
              break;
            case 'pause':
              status = 3;
              break;
            case 'end':
              status = 4;
              break;
          }
          media.onStatusChange && media.onStatusChange(status, !event['jsInvoke'] || !!event.isTrusted);
        };

        media.addEventListener('play', handler);
        media.addEventListener('pause', handler);
        media.addEventListener('ended', handler);

        return resultObj;
      }
    }
  }])

  .factory('$audioWrap', ['$audio', '$cordovaMedia', '$q',
    function ($audio, $cordovaMedia, $q) {
      var supportPauseRecord = ionic.Platform.isIOS();
      return {
        newMedia: function (filePath, forceDuration, afterStopRecord) {
          if (!window.Media) return;
          var media;
          if (!filePath) {
            media = $audio.newMedia();
          } else {
            media = $audio.newMedia(filePath);
          }
          var mimeType = media.getMimeType(),
            mediaFileNames = [],
            mediaFileName,
            duration = 0,
            startRecordTs,
            returnObj;

          returnObj = {
            play: function (options) {
              media.play(options);
            },
            pause: function () {
              media.pause();
            },
            stop: function () {
              media.stop();
            },
            release: function () {
              media.release();
            },
            seekTo: function (timing) {
              media.seekTo(timing);
            },
            setVolume: function (volume) {
              media.setVolume(volume);
            },
            startRecord: function () {
              media.startRecord();
              startRecordTs = Date.now();
            },
            pauseRecord: function () {
              console.log("pauseRecord");
              if (supportPauseRecord) {
                console.log("pauseRecord");
                media.pauseRecord();
              } else {
                var thisMediaFileName = media.getFileName();
                if (_.indexOf(mediaFileNames, thisMediaFileName) > -1) {
                  return;
                }
                media.stopRecord();
                duration = duration + Date.now() - startRecordTs;
                mediaFileNames.push(thisMediaFileName);
                media.release();
              }
            },
            resumeRecord: function () {
              if (supportPauseRecord) {
                media.resumeRecord();
              } else {
                media = $audio.newMedia();
                media.startRecord();
                startRecordTs = Date.now();
              }
            },
            restartRecord: function () {
              media.stopRecord();
              mediaFileNames.length = 0;
              duration = 0;
              returnObj.startRecord();
            },
            cancelRecord: function () {
              media.stopRecord();
              media.release();
            },
            stopRecord: function () {
              var defer = $q.defer();
              var thisMediaFileName = media.getFileName();
              if (_.indexOf(mediaFileNames, thisMediaFileName) == -1) {
                media.stopRecord();
                media.release();
                duration = duration + Date.now() - startRecordTs;
                mediaFileNames.push(thisMediaFileName);
              }
              mediaFileName = mediaFileNames.shift();
              if (mediaFileNames.length > 0) {
                console.log("begin audio join");
                window.audioJoin(mediaFileName, mediaFileNames, function () {
                  if (forceDuration) {
                    $audio.newMedia(mediaFileName).getDuration().then(function (_duration) {
                      var dur = _duration;
                      if (dur <= 0) {
                        dur = duration / 1000;
                      }
                      defer.resolve({fullPath: mediaFileName, duration: dur, mimeType: mimeType});
                    }, function () {
                      defer.resolve({
                        fullPath: mediaFileName,
                        duration: duration / 1000,
                        mimeType: mimeType
                      });
                    });
                  } else {
                    defer.resolve({
                      fullPath: mediaFileName,
                      duration: duration / 1000,
                      mimeType: mimeType
                    });
                  }
                }, function () {
                  defer.reject("audio join failed");
                });
              } else {
                if (forceDuration) {
                  $audio.newMedia(mediaFileName).getDuration().then(function (_duration) {
                    var dur = _duration;
                    if (dur <= 0) {
                      dur = duration / 1000;
                    }
                    defer.resolve({fullPath: mediaFileName, duration: dur, mimeType: mimeType});
                  }, function () {
                    defer.resolve({
                      fullPath: mediaFileName,
                      duration: duration / 1000,
                      mimeType: mimeType
                    });
                  });
                } else {
                  defer.resolve({
                    fullPath: mediaFileName,
                    duration: duration / 1000,
                    mimeType: mimeType
                  });
                }
              }
              console.log("stopRecord");
              return defer.promise;
            }
          };

          return returnObj;
        }
      };
    }
  ]);
