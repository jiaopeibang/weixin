'use strict';

angular.module('jpb.common')
	.factory('$notification', ['$ionicModal', '$timeout', '$rootScope',
		function ($ionicModal, $timeout, $rootScope) {
			var modal, $el, $scope = $rootScope.$new();

			function loadModal(width) {
				if (!modal) {
					modal = $ionicModal.fromTemplate(
						'<div class="message-prompt" ' + '>{{text}}</div>', {
							scope: $scope,
							animation: 'none'
						}
					);
					$el = $(modal.el);
					$el.css({'background': 'rgba(0,0,0,0)', 'z-index': 99999});
				}
				$el.find('.message-prompt').css('width', width);
			}

			function generateWidth(text) {
				var width = (text.replace(/[^\x00-\xff]/g, 'xx')).length * 8 + 36;
				if (width >= 300) {
					width = 300;
				} else if (width <= 160) {
					width = 160;
				}
				return width;
			}

			return {
				show: function (text, time) {
					var width = generateWidth(text);
					$scope.text = text;
					loadModal(width);
					modal.show();
					if (time) {
						$timeout(function () {
							modal.hide();
						}, time);
					}
				},
				hide: function () {
					if (!!modal) {
						modal.hide();
					}
				}
			}
		}
	]);
