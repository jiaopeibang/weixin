/*!
 * ngCordova
 * v0.1.20-alpha
 * Copyright 2015 Drifty Co. http://drifty.com/
 * See LICENSE in this repository for license information
 */
(function(){

	angular.module('ngCordova', [
		'ngCordova.plugins'
	]);

	angular.module('ngCordova.plugins', [
		'ngCordova.plugins.media'
	]);

	angular.module('ngCordova.plugins.media', [])

		.factory('$cordovaMedia', ['$q', function ($q) {

			return {
				newMedia: function (src) {
					var q = $q.defer();
					var mediaStatus = null;
					var media;
					media = new Media(src,
						function (success) {
							q.resolve(success);
						}, function (error) {
							q.reject(error);
						}, function (status) {
							mediaStatus = status;
							q.promise.onStatusChange && q.promise.onStatusChange(mediaStatus);
						});
					// getCurrentPosition NOT WORKING!
					q.promise.getCurrentPosition = function () {
						media.getCurrentPosition(function (success) {
						}, function (error) {
						});
					};

					q.promise.getDuration = function () {
						return media.getDuration();
					};

					q.promise.getStatus = function () {
						return mediaStatus;
					};

					// iOS quirks :
					// -  myMedia.play({ numberOfLoops: 2 }) -> looping
					// -  myMedia.play({ playAudioWhenScreenIsLocked : false })
					q.promise.play = function (options) {
						if (typeof options !== "object") {
							options = {};
						}
						media.play(options);
					};

					q.promise.pause = function () {
						media.pause();
					};

					q.promise.stop = function () {
						media.stop();
					};

					q.promise.release = function () {
						media.release();
					};

					q.promise.seekTo = function (timing) {
						media.seekTo(timing);
					};

					q.promise.setVolume = function (volume) {
						media.setVolume(volume);
					};

					q.promise.startRecord = function () {
						media.startRecord();
					};

					q.promise.pauseRecord = function () {
						media.pauseRecord();
					};

					q.promise.resumeRecord = function () {
						media.resumeRecord();
					};

					q.promise.stopRecord = function () {
						media.stopRecord();
					};

					q.promise.media = media;
					return q.promise;
				}
			};
		}]);
})();