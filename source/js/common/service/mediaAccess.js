'use strict';

angular.module('jpb.common')

	.factory('$mediaAccess', ['$rootScope',
		function () {
			var serviceObj = {
				getMediaUrl: function (obj, type, style) {
					var server = serviceconfig.qiniu[type].domain,
						objId,
						url;

					if (angular.isObject(obj)) {
						objId = obj.referenceId || obj._id;
					} else {
						objId = obj;
					}

					url = server + "/" + objId;

					if (style) {
						url = url + "-" + style;
					}
					// TODO use local default image.

					return url;
				},
				getAudioUrl: function (obj) {
					return serviceObj.getMediaUrl(obj, 'audio');
				}
			};

			return serviceObj;
		}
	]);
