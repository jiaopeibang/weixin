'use strict';

angular.module('jpb.common')

  .factory('$localStorage', ['$window',
    function ($window) {
      return {
        set: function (key, value) {
          $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
          return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
          $window.localStorage[key] = angular.toJson(value);
        },
        getObject: function (key, defaultValue) {
          return angular.fromJson($window.localStorage[key] || defaultValue);
        },
        length: function () {
          return $window.localStorage.length;
        },
        key: function (index) {
          return $window.localStorage.key(index);
        },
        removeItem: function (key) {
          $window.localStorage.removeItem(key);
        }
      }
    }
  ]);
