'use strict';

angular.module('jpb.common')

  .factory('$cacheCheck', ['$rootScope', '$localStorage', '$http',
    function ($rootScope, $localStorage, $http) {

      var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;

      function check() {
        var latestCacheTs = $localStorage.get('_latestCacheTs', 0);

        if (!latestCacheTs || Date.now() - latestCacheTs > 24 * 60 * 60 * 1000) {
          var promise = $http.get(server + '/api/caches?latestCacheTs=' + latestCacheTs);
          promise.then(function (response) {
            var result = response.data;
            if (result.result == 'SUCCESS') {
              $localStorage.set('_latestCacheTs', Date.now());
              // clear related cache per
              for (var key in result.data) {
                if (result.data.hasOwnProperty(key)) {
                  removeCache(key);
                }
              }
            } else {
              alert('数据加载异常');
            }
          });
        }
      }

      function removeCache(value) {
        var key, length = $localStorage.length();

        for (var i = length - 1; i > 0; i--) {
          key = $localStorage.key(i) || '';
          if (key.indexOf(value) === 0) {
            $localStorage.removeItem(key);
          }
        }

        if (value === 'lookupValue') {
          var getLookupValues = $http.get(server + '/api/lookupValues');
          getLookupValues.then(function (response) {
            var result = response.data;
            $rootScope.LookupValueObj = result.data;
            $localStorage.setObject('lookupValues', result.data);
          }, function (err) {
            console.error(err);
          });
        }
      }

      return {
        check: check
      }
    }]);
