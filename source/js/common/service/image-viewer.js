'use strict';

angular.module('jpb.common')

  .factory('$imageViewer', ['$rootScope', '$ionicModal', '$ionicPlatform',
    function ($rootScope, $ionicModal, $ionicPlatform) {
      var newScope = $rootScope.$new();
      newScope.showTop = false;

      function open(imageUrl, id, context, params) {
        newScope.showTop = false;
        newScope.imageUrl = imageUrl;
        $ionicModal.fromTemplateUrl('templates/component/image-viewer.html', {
          scope: newScope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          if (params) {
            angular.extend(newScope, params);
          }
          newScope.imageModal = modal;
          newScope.imageModal.show();
          newScope.hide = function () {
            newScope.showTop = false;
            newScope.imageModal.hide();
          };

          newScope.sendWrfMessage = function (url) {
            newScope.hide();
          }
        });
      }

      angular.extend(newScope, {
        editImage: function () {
          //newScope.showTop = !newScope.showTop;
          newScope.hide();
        },

        hideImage: function () {
          newScope.hide();
        }
      });

      return {
        open: open
      };
    }
  ]);
