'use strict';

angular.module('jpb.common')
  .filter('LookupValue', function ($rootScope) {
    return function (value, type, valuefield) {
      valuefield = valuefield || 'key';
      var list = $rootScope.LookupValueObj && $rootScope.LookupValueObj[type];
      var where = {};
      where[valuefield] = value;
      var lv = _.findWhere(list, where);
      return lv && lv.label;
    };
  })

  .filter('LookupValueHead', function ($rootScope) {
    return function (value, type, valuefield) {
      valuefield = valuefield || 'key';
      var list = $rootScope.LookupValueObj && $rootScope.LookupValueObj[type];
      var where = {};
      var subject;
      where[valuefield] = value;
      var lv = _.findWhere(list, where);
      subject = lv.label;
      if (lv.label !== '研究生') {
        subject = lv.label.substr(0, 2);
      }
      return subject;
    };
  })

  .filter('gradeFilter', function () {
    return function (grades, type) {
      return grades.filter(function (item) {
        if (type == 'teacher') {
          return item.value > 100;
        } else {
          return item.value < 100;
        }
      });
    }
  });
