'use strict';

angular.module('jpb.common')
	.filter('calendarTime', function () {// TODO merge all date-time related filters here
		return function (dt) {
			if (dt) {
				return moment(dt).calendar();
			}
			return '';
		};
	})

	//作业首页时间过滤
	.filter('calendarTimeIndex', function () {// TODO merge all date-time related filters here
		return function (dt) {
			if (dt) {
				return moment(dt).calendarIndex();
			}
			return '';
		};
	})

	.filter('dateFormat', function () {
		return function (dt, format) {
			if (dt) {
				return moment(dt).format(format);
			}
			return '';
		};
	})
	.filter('weekDay', function(){
		return function(dt) {
			if(dt) {
        moment.locale('zh-cn');
				return moment.weekdays(moment(dt).day());
			}

			return '';
		}
	})

;
