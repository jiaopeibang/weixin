'use strict';

angular.module('jpb.common')
	.filter('duration', function () {
		return function (duration) {
			if (duration > 0) {
				var hours = Math.floor(duration / 3600);
				var display;
				if (hours > 0) {
					display = hours + ":";
					duration = duration % 3600;
				}
				var minutes = Math.floor(duration / 60);
				if (minutes > 0) {
					if (display) {
						display += ((minutes || 0) + "'");
					} else {
						display = ((minutes || 0) + "'");
					}
				}
				var seconds = Math.floor(duration % 60);
				if (display) {
					display += ((seconds || 0) + '"');
				} else {
					display = (seconds || 0) + '"';
				}
				return display;
			}

			return '0"';
		};
	})

	.filter('mediaUrl', function () {
		return function (obj, type, style) {
			var server = serviceconfig.qiniu[type].domain,
				objId,
				url;

			if (angular.isObject(obj)) {
				objId = obj.referenceId || obj._id;

				if (obj.msgType === 'image' && obj._$$localPath) {
					return obj._$$localPath;
				}
			} else {
				objId = obj;
			}

			url = server + '/' + objId;

			if (style) {
				return url + '-' + style;
			}

			// TODO use local default image.

			return url;
		};
	})

	.filter('photoUrl', function ($filter) {
		return function (obj, style) {
			return $filter('mediaUrl')(obj, 'photo', style);
		};
	})

	.filter('photoThumbUrl', function ($filter) {
		return function (obj) {
			return $filter('photoUrl')(obj, 'thumb');
		};
	})

	.filter('wrfPhotoUrl', function ($filter) {
		return function (obj, style) {
			return $filter('mediaUrl')(obj, 'wrfsnap', style);
		};
	})

	.filter('wrfPhotoThumbUrl', function ($filter) {
		return function (obj) {
			return $filter('wrfPhotoUrl')(obj, 'thumb');
		};
	})

  .filter('photoNormalUrl', function ($filter) {
    return function (obj) {
      var url = $filter('photoUrl')(obj);
      url += '?imageMogr2/auto-orient/thumbnail/1920x1920';
      return url;
    };
  })

  .filter('photoCropUrl', function($filter){
    return function (obj, options) {
      //rotate/90/crop/!826x859a154a350
      var url= $filter('photoNormalUrl')(obj);
      url += '/rotate/'+ options.rotate +'/crop/!' +
        options.width + 'x' + options.height +
        'a' + options.x + 'a' + options.y;
      return url;
    };
  })

	.filter('certificateUrl', function ($filter) {
		return function (user, type, style) {
			var ext,
				url,
				id = user;

			if (angular.isObject(user)) {
				var certificateModified = Jpb.util.byPath('profile.certificates.' + type + '.modified', user);
				if (certificateModified) {
					ext = certificateModified;
				}
				id = user._id;
			}

			if (type) {
				id = id + '/certificates/' + type;
			}

			url = $filter('mediaUrl')(id, 'photo', style);

			if (ext) {
				url = url + '?' + ext;
			}

			return url;
		};
	})

	.filter('defaultAvatarUrl', function () {
		return function (type) {
			return 'img/avatar/' + type + '.png';
		};
	})

	.filter('avatarUrl', function ($filter) {
		return function (user, style) {
			var ext,
				url;

			if (angular.isObject(user)) {
				var avatarModified = Jpb.util.byPath('profile.avatar.modified', user);

				if (avatarModified) {
					ext = avatarModified;
				} else {
					return 'img/avatar/' + user.profile.type + '.png';
				}
			} else {
				console.log(user);
			}

			url = $filter('mediaUrl')(user, 'avatar', style);
			if (ext) {
				url = url + '?' + ext;
			}

			return url;
		};
	})

	.filter('certificateType', function () {
		return function (type) {
			if (type === 'id') {
				return 'icon-identity-card-a';
			} else if (type === 'idVerso') {
				return 'icon-identity-card-b';
			} else {
				return 'icon-image';
			}
		}
	});
