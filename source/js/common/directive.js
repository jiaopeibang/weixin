(function (angular) {
	'use strict';

	var checkMandatory = function (prop, desc) {
		if (!prop) {
			throw new Error(desc);
		}
	};

	var defaults = function (dest, src) {
		for (var key in src) {
			if (typeof dest[key] === 'undefined') {
				dest[key] = src[key];
			}
		}
	};

	/**
	 * Construction function
	 *
	 * @constructor
	 */
	angular.module("jpb.common").directive('star', function () {
		return {
			restrict: 'EA',
			transclude: true,
			template: '<img ng-repeat="star in $stars" src="img/qr-code/qr-code.png" style="width: 14px;"/>',
			controller: function ($scope) {
				$scope.compute = function (count) {
					var sunCount = count / 5;
					var starCount = count % 5;
					var stars = [];
					for (var i = 0; i < starCount; i++) {
						stars.push(i);
					}
					$scope.$stars = stars;
				};
			},
			link: function ($scope, $element, attributes) {
				$scope.compute(parseInt(attributes.starcount));
			}
		};
	}).directive('focusMe', ['$timeout','$parse',function($timeout, $parse) {
		return {
			// scope: true, // optionally create a child scope
			link : function(scope, element, attrs) {
				var model = $parse(attrs.focusMe);
				scope.$watch(model, function(value) {
					if (value === true) {
						$timeout(function() {
							element[0].focus();
						}, 600);
					}
				});
				// to address @blesh's comment, set attribute value to
				// 'false'
				// on blur event:
				element.bind('blur', function() {
					if(model.assign){
						scope.$apply(model.assign(scope, false));
					}
				});
			}
		};
	}]).directive('clickOnce', function($timeout) {
		return {
			restrict : 'A',
			link : function(scope, element, attrs) {
				var replacementText = attrs.clickOnce;

				element.bind('click', function() {
					$timeout(function() {
						if (replacementText) {
							element.html(replacementText);
						}
						element.attr('disabled', true);
					}, 0);
				});
			}
		};
	}).directive('srMutexClick', function ($parse) {
		return {
			compile: function ($element, attr) {
				var fn = $parse(attr['srMutexClick']);
				return function srEventHandler(scope, element) {
					var submitting = false;
					element.on('click', function (event) {
						scope.$apply(function () {
							if (submitting) {
								return;
							}
							submitting = true;
							fn(scope, {$event: event}).finally(function() { submitting = false });
						});
					});
				};
			}
		};
	}).directive("timeConvert", function() {
		return {
			require: "ngModel",
			link: function(scope, elm, attrs, ctrl) {
				ctrl.$parsers.push(function(viewValue) {
					if (viewValue) {
						return viewValue.getTime();
					}
				});
				ctrl.$formatters.unshift(function(modelValue) {
					if (modelValue) {
						return new Date(modelValue);
					}
					return modelValue;
				});
			}
		};
	}).directive("timestampConvert", function() {
		return {
			require: "ngModel",
			link: function(scope, elm, attrs, ctrl) {
				ctrl.$parsers.push(function(viewValue) {
					console.log(viewValue);
					if (viewValue) {
						console.log(viewValue);
						return viewValue.getTime();
					}
				});
				ctrl.$formatters.unshift(function(modelValue) {
					console.log(modelValue);
					if (modelValue) {
						return new Date(parseInt(modelValue, 10));
					}
					return modelValue;
				});
			}
		};
	}).directive('ngEnter', function () {
		return function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				if (event.which === 13) {
					scope.$apply(function () {
						scope.$eval(attrs.ngEnter);
					});

					event.preventDefault();
				}
			});
		};
	});
}(angular));
