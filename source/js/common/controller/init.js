'use strict';

angular.module('jpb.common')

  .controller('InitController', ['$state', '$ionicHistory',
    function ($state, $ionicHistory) {
      $ionicHistory.nextViewOptions({
        disableBack: true,
        historyRoot: true
      });

       //$state.go('demo');
    }
  ]);
