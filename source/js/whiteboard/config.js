angular

  .module("whiteboard")

  .config(['$stateProvider',
    function ($stateProvider) {
      "use strict";

      $stateProvider
        .state('newPlayer', {
          url: '/newPlayer/:id?qId&aId',
          templateUrl: 'templates/whiteboard/player.html',
          controller: 'NewPlayerCtrl',
          data: {
            acl: {
              signIn: false
            }
          }
        });
    }]);
