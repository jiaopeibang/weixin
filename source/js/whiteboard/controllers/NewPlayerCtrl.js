angular.module("whiteboard")

  .controller('NewPlayerCtrl', ['$scope', '$http', '$WBPlayer', '$stateParams', '$audio', '$filter', '$ionicPopup',
    function ($scope, $http, $WBPlayer, $stateParams, $audio, $filter, $ionicPopup) {
      $scope.isPublic = true;
      $scope.isShow = false;
      document.title = "讲解";
      var urlPrefix = window.__server_runtime_config._DEFAULT_CONNECTION_URL + '/api';

      //获取需要评价的message
      var questionId = $stateParams.qId,//问题
        answerId = $stateParams.aId,//答案
        selectQuestion = {_id: questionId},
        selectAnswer = {_id: answerId},
        selectNotifications = {messageId: questionId},
        evaluate,
        answerEvaluate,
        evaluateObj;

      function setFeedBack(popup) {
        /**
         * @summary wrf feedback, update question message, generate notifications to teacher
         * @param {Object} params
         * @param {Object} params.questionSelector  find question; not empty
         * @param {Object} params.questionUpdater
         * @param {Object} params.answerSelector  find Answer; not empty
         * @param {Object} params.answerUpdater
         * @param {Object} params.notificationSelector notifications finder; not empty
         * @param {Object} params.notificationsUpdater upsert
         */
        $http.post(urlPrefix + '/wrf/feedback', {
          questionSelector: selectQuestion,
          questionUpdater: evaluate,
          answerSelector: selectAnswer,
          answerUpdater: answerEvaluate,
          notificationSelector: selectNotifications,
          notificationsUpdater: evaluateObj
        });
        $scope.isShow = false;
        popup.close();
      }


      //获取该条message的信息
      $http.get(urlPrefix + '/message/' + questionId)
        .then(function (response) {
          var result = response.data || {};
          $scope.message = $scope.question = result.data || {};
        });

      $http.get(urlPrefix + '/message/' + answerId).then(function (response) {
        var result = response.data || {};
        $scope.answer = result.data || {};
      });

      $http.get(urlPrefix + '/media/' + $stateParams.id).then(function (res) {
        var result = res.data.data;//api请求结果中取出data
        var data = result.data;
        if (data.bgImageId) {
          if (data.cropData) {
            data.bgImageUrl = $filter('photoCropUrl')({
              _id: data.bgImageId,
              type: 'image'
            }, data.cropData);
          } else {
            data.bgImageUrl = $filter('mediaUrl')(data.bgImageId, 'photo');
          }
        }

        if (data.wbBgImageId) {
          data.wbBgImageUrl = $filter('mediaUrl')(data.wbBgImageId, 'photo');
        }
        var conf = {};
        if (result.mediaInfo) {
          var mediaUrl = $filter('mediaUrl')(result.mediaInfo._id, 'audio');
          conf.media = $audio.newMedia(mediaUrl);
          //	conf.duration = result.mediaInfo.duration;
        }

        var config = angular.extend({
          canvasId: 'pwhiteboard',
          qcanvasId: 'pqwhiteboard',
          playerId: 'wbplayer',
          loadingHandler: function (isLoading) {
            $scope.$apply(function () {
              $scope.isLoading = isLoading;
            });
          }
        }, conf);
        var player = new WB.Player(config, data);

        WB.Topic.subscribe('videoEnd', function () {
          if (!$scope.answer.answerEvaluate) {
            $scope.isShow = true;
            answerEvaluate = {'answerEvaluate': true};
            var content;

            var myPopup = $ionicPopup.show({
              title: '这题你听懂了吗',
              cssClass: 'popup-show',
              buttons: [{
                text: '1懂了,谢谢老师',
                type: 'button-default',
                onTap: function () {
                  $scope.answer.answerEvaluate = true;
                  evaluate = {'evaluate': '懂了,谢谢老师'};
                  content = $scope.message.from.name.toString() + '看懂了您的讲解';
                  evaluateObj = {
                    type: 'feedback',
                    messageId: questionId,
                    content: content,
                    contentType: '懂了，谢谢老师',
                    status: 0,
                    createdAt: new Date(),
                    session: $scope.message.from.id
                  };
                  setFeedBack(myPopup);
                }
              }, {
                text: '2用到的知识没学过',
                type: 'button-default',
                onTap: function () {
                  $scope.answer.answerEvaluate = true;
                  evaluate = {'evaluate': '用到的知识没学过'};
                  content = $scope.message.from.name.toString() + '对讲解有疑问，请前往查看';
                  evaluateObj = {
                    type: 'feedback',
                    messageId: questionId,
                    content: content,
                    contentType: '懂了，谢谢老师',
                    status: 0,
                    createdAt: new Date(),
                    session: $scope.message.from.id
                  };
                  setFeedBack(myPopup);
                }
              }, {
                text: '3解题的推理过程不理解',
                type: 'button-default',
                onTap: function () {
                  $scope.answer.answerEvaluate = true;
                  evaluate = {'evaluate': '解题的推理过程不理解'};
                  content = $scope.message.from.name.toString() + '对讲解有疑问，请前往查看';
                  evaluateObj = {
                    type: 'feedback',
                    messageId: questionId,
                    content: content,
                    contentType: '解题的推理过程不理解',
                    status: 0,
                    createdAt: new Date(),
                    session: $scope.message.from.id
                  };
                  setFeedBack(myPopup);
                }
              }, {
                text: '4其他',
                type: 'button-default',
                onTap: function () {
                  $scope.answer.answerEvaluate = true;
                  evaluate = {'evaluate': '其他'};
                  content = $scope.message.from.name.toString() + '对讲解有疑问，请前往查看';
                  evaluateObj = {
                    type: 'feedback',
                    messageId: questionId,
                    content: content,
                    contentType: '其他',
                    status: 0,
                    createdAt: new Date(),
                    session: $scope.message.from.id
                  };
                  setFeedBack(myPopup);
                }
              }, {
                text: '5答案错了',
                type: 'button-default',
                onTap: function () {
                  $scope.answer.answerEvaluate = true;
                  evaluate = {'evaluate': '答案错了'};
                  content = $scope.message.from.name.toString() + '对讲解有疑问，请前往查看';
                  evaluateObj = {
                    type: 'feedback',
                    messageId: questionId,
                    content: content,
                    contentType: '答案错了',
                    status: 0,
                    createdAt: new Date(),
                    session: $scope.message.from.id
                  };
                  setFeedBack(myPopup);
                }
              }]
            });
          }
        });

        $scope.$on('$destroy', function () {
          player && player.release();
        });
      });
    }]);

