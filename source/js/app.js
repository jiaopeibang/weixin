angular.module('jpb', [
    'ionic',
    'ngCordova',
    'jpb.common',
    'jpb.weixin',
    'timer',
    'whiteboard'
  ])

  .run(['$ionicPlatform', '$rootScope', '$localStorage', '$cacheCheck', '$http',
    function ($ionicPlatform, $rootScope, $localStorage, $cacheCheck, $http) {
      $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }
      });

      $cacheCheck.check();

      //获取常用数据
      var lookupValues = $localStorage.getObject('lookupValues');
      if (lookupValues && Object.keys(lookupValues).length > 0) {
        return $rootScope.LookupValueObj = angular.fromJson(lookupValues);
      }
      var server = window.__server_runtime_config._DEFAULT_CONNECTION_URL;
      var getLookupValues = $http.get(server + '/api/lookupValues');
      getLookupValues.then(function (response) {
        var result = response.data;
        if (result.result === 'SUCCESS') {
          $rootScope.LookupValueObj = result.data;
          $localStorage.setObject('lookupValues', result.data);
        }
      }, function (err) {
        console.error(err);
      });
    }
  ]);
