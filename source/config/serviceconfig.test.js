serviceconfig.qiniu = {
  uploadServer: {
    url: 'http://upload.qiniu.com/'
  },
  image: {
    service: 'image',
    bucket: 'image',
    domain: 'http://7xo8bq.com1.z0.glb.clouddn.com'
  },
  avatar: {
    service: 'avatar',
    bucket: 'avatar',
    domain: 'http://7xo8bm.com1.z0.glb.clouddn.com'
  },
  photo: {
    service: 'photo',
    bucket: 'photo',
    domain: 'http://7xo8bo.com1.z0.glb.clouddn.com'
  },
  audio: {
    service: 'audio',
    originbucket: 'jpb-origin-audio',
    bucket: 'audio',
    domain: 'http://7xo8bn.com1.z0.glb.clouddn.com'
  },
  wrfsnap: {
    service: 'wrfsnap',
    bucket: 'wrfsnap',
    domain: 'http://7xo8br.com1.z0.glb.clouddn.com'
  }
};
