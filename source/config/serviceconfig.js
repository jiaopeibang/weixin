var serviceconfig = {
  baidutongji: {
    token: "@fixme"
  },
  qiniu: {
    uploadServer: {
      url: 'http://upload.qiniu.com/'
    },
    image: {
      service: 'image',
      bucket: 'jpb-image',
      domain: 'http://image.jiaopeibang.com'
    },
    avatar: {
      service: 'avatar',
      bucket: 'jpb-avatar',
      domain: 'http://avatar.jiaopeibang.com'
    },
    photo: {
      service: 'photo',
      bucket: 'jpb-photo',
      domain: 'http://photo.jiaopeibang.com'
    },
    audio: {
      service: 'audio',
      originbucket: 'jpb-origin-audio',
      bucket: 'audio',
      domain: 'http://audio.jiaopeibang.com'
    },
    wrfsnap: {
      service: 'wrfsnap',
      bucket: 'jpb-wrfsnap',
      domain: 'http://wrfsnap.jiaopeibang.com'
    }
  }
};

window.umappkey = '54b67a24fd98c51b1300069a';
