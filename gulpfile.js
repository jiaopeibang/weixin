var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var env = process.env.NODE_ENV || "development";//另一个production，是否压缩
var mode = process.env.MODE || 'local';//另一个值test

var prefix = './source/';
// provide base paths
var paths = {
  base: prefix,
  sassApp: prefix + 'scss/**',
  copyImg: prefix + 'img/**',
  jsConfig: prefix + 'config/',
  jsApp: prefix + 'js/**',
  copyTemplates: prefix + 'templates/**',
  jsLib: prefix + 'lib/'
};

// path detail
var paths_dtl = {
  jsLib: [
    paths.jsLib + 'ionic-service-core/ionic-core.js',
    paths.jsLib + 'angular-timer/dist/angular-timer.js',
    paths.jsLib + 'humanize-duration/humanize-duration.js',
    paths.jsLib + 'momentjs/moment.js',
    paths.jsLib + 'momentjs/locale/zh-cn.js',
    //paths.jsLib + 'ngstorage/ngStorage.min.js',
    paths.jsLib + 'jquery/dist/jquery.min.js',
    paths.jsLib + 'underscore/underscore-min.js',
    paths.jsLib + 'hammerjs/hammer.js',
    paths.jsLib + 'whiteboard/dist/whiteboard_player.js'
  ],
  cssLib: [
    paths.jsLib + 'whiteboard/dist/css/whiteboard.css'
  ],
  templateLib: [
    paths.jsLib + 'whiteboard/dist/templates/**'
  ]
};


gulp.task('jsConfig', function () {
  var srcPath = ['server.config.js', 'serviceconfig.js'];
  if (mode === 'local')
    srcPath.push('*.local.js');
  else if (mode === 'test')
    srcPath.push('*.test.js');
  srcPath = srcPath.map(function (item) {
    return paths.jsConfig + item;
  });
  var pipeResult = gulp.src(srcPath)
    .pipe(concat('config.js'));
  if (env === 'production') {
    pipeResult = pipeResult.pipe(uglify({mangle: false}));
  }
  pipeResult.pipe(gulp.dest('./www/js'));
});

gulp.task('jsLib', function () {
  var pipeResult = gulp.src(paths_dtl.jsLib)
    .pipe(concat('lib.js'));
  if (env === 'production') {
    pipeResult = pipeResult.pipe(uglify({mangle: true}));
  }
  return pipeResult.pipe(gulp.dest('./www/js'));
});

gulp.task('jsApp', function () {
  var pipeResult = gulp.src(paths.base + 'js/**/*.js')
    .pipe(concat('app.js'));
  if (env === 'production') {
    pipeResult = pipeResult.pipe(uglify({mangle: false}));
  }
  pipeResult.pipe(gulp.dest('./www/js'));
});

// lib css
gulp.task('cssLib', function (done) {
  gulp.src(paths_dtl.cssLib)
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(concat('lib.css'))
    .pipe(gulp.dest('./www/css'))
    .on('end', done);
});

// app style
gulp.task('sassApp', function (done) {
  var pipeResult = gulp.src(paths.base + 'scss/app.scss')
    .pipe(sass({
      errLogToConsole: true
    }));
  if (env === 'production') {
    pipeResult = pipeResult.pipe(minifyCss({
      keepSpecialComments: 0
    }));
  }
  pipeResult.pipe(rename({extname: '.css'}))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

// ionic default style
gulp.task('sassIonic', function (done) {
  var pipeResult = gulp.src(paths.base + 'scss/ionic.app.scss')
    .pipe(sass({
      errLogToConsole: true
    }));
  if (env === 'production') {
    pipeResult = pipeResult.pipe(minifyCss({
      keepSpecialComments: 0
    }));
  }
  pipeResult.pipe(rename({extname: '.css'}))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});

gulp.task('copyJs', function () {
  var pipeResult = gulp.src(paths.base + 'lib/ionic/js/ionic.bundle.js');
  if (env === 'production') {
    pipeResult = gulp.src(paths.base + 'lib/ionic/js/ionic.bundle.min.js');
  }
  pipeResult.pipe(rename('ionic.bundle.min.js'));
  pipeResult.pipe(gulp.dest('./www/js/'));
});

gulp.task('copyImg', function () {
  gulp.src(paths.copyImg)
    .pipe(gulp.dest('./www/img'));
});

gulp.task('copyTemplates', function () {
  gulp.src([paths.base + 'templates/**', '!' + paths.base + 'templates/*.html'])
    .pipe(gulp.dest('./www/templates'));
  gulp.src(paths_dtl.templateLib)
    .pipe(gulp.dest('./www/templates'));
  gulp.src(paths.base + 'templates/*.html')
    .pipe(gulp.dest('./www/'));
});

gulp.task('copyFonts', function () {
  gulp.src([paths.base + 'lib/ionic/fonts/*', paths.base + 'fonts/*'])
    .pipe(gulp.dest('./www/fonts'));
});

var tasks = {
  copy: ['copyTemplates', 'copyJs', 'copyImg', 'copyFonts'],
  css: ['sassIonic', 'sassApp', 'cssLib'],
  js: ['jsConfig', 'jsApp', 'jsLib']
};
tasks.default = tasks.copy.concat(tasks.css).concat(tasks.js);

gulp.task('default', tasks.default);
gulp.task('copy', tasks.copy);
gulp.task('css', tasks.css);
gulp.task('js', tasks.js);

gulp.task('watch', function () {
  gulp.watch(paths.jsConfig, ['jsConfig']);
  gulp.watch(paths.jsApp, ['jsApp']);
  gulp.watch(paths.sassApp, ['sassApp']);
  gulp.watch(paths.copyImg, ['copyImg']);
  gulp.watch(paths.copyTemplates, ['copyTemplates']);
});

gulp.task('install', ['git-check'], function () {
  return bower.commands.install()
    .on('log', function (data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function (done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});
