####微信端运行要点：
1. 保证source/config/目录下配置正确
2. npm install, bower install, gulp

####新公众号配置运行注意点：
1. 公众号消息回调 {jpb server domain}/weixin/message?appid={appid}
2. 创建机构，正确填入微信开发所需开发者配置信息
3. 接口权限处配置‘网页授权获取用户信息’为本服务域名,注意不含http(s)
4. 配置支付目录，不然无法调起支付！
以下步骤可选：
5. 服务器端生成适合的菜单
6. 服务器端配置正确的支付回调url和contextPath（即本服务域名）

注：所有domain后面均不带'/'